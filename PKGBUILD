# Maintainer: Andreas 'Segaja' Schleifer <segaja at archlinux dot org>

pkgname=stern
pkgdesc="Multi pod and container log tailing for Kubernetes"
pkgver=1.24.0
_github_project='stern/stern'
pkgrel=1
arch=('x86_64')
url="https://github.com/${_github_project}"
license=('apache')
options=(!lto)
depends=('glibc')
makedepends=('go')
optdepends=('bash-completion: for bash completion support')
source=("${url}/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz")
sha512sums=('a27a0d18e364a6a69992bf4ce04657a5ec20365b4e7fd166d905aefa7064f0e35d96da5954e42361caea0f58da1c858ee13dda5de9c4a7dfe9c8eaa869fdd828')
b2sums=('827f1d251c481fee8e81a3496dd45177384689aebb846d47d36c877044717484b3dfaa8449e2619e437c40438fddbc88b0e8a57783cb16180b269af9984bcc45')

build() {
  cd "${pkgname}-${pkgver}"

  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw"

  go build -v -x \
    -ldflags "-linkmode external -X \"github.com/stern/stern/cmd.version=${pkgver}\"" \
    -o "./out/${pkgname}"
}

check() {
  cd "${pkgname}-${pkgver}"

  go test -v ./...
}

package() {
  cd "${pkgname}-${pkgver}"

  install --verbose -D --mode=0755 "./out/${pkgname}" "${pkgdir}/usr/bin/${pkgname}"

  "${pkgdir}/usr/bin/${pkgname}" --completion=bash | install --verbose -D --mode=0644 /dev/stdin "${pkgdir}/usr/share/bash-completion/completions/${pkgname}"
  "${pkgdir}/usr/bin/${pkgname}" --completion=fish | install --verbose -D --mode=0644 /dev/stdin "${pkgdir}/usr/share/fish/vendor_completions.d/${pkgname}.fish"
  "${pkgdir}/usr/bin/${pkgname}" --completion=zsh | install --verbose -D --mode=0644 /dev/stdin "${pkgdir}/usr/share/zsh/site-functions/_${pkgname}"

  install --verbose -D --mode=0644 *.md --target-directory "${pkgdir}/usr/share/doc/${pkgname}"
}
